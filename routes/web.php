<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* Creational Patterns */
Route::get('/builder','HomeController@builderPattern');
Route::get('/factory','HomeController@factoryPattern');
Route::get('/prototype','HomeController@prototypePattern');
Route::get('/singleton','HomeController@singletonPattern');
Route::get('/factory/method','HomeController@factoryMethodPattern');

/* Structural Patterns */
Route::get('/adapter','HomeController@adapterPattern');
Route::get('/bridge','HomeController@bridgePattern');
Route::get('/composite','HomeController@compositePattern');
Route::get('/decorator','HomeController@decoratorPattern');
Route::get('/facade','HomeController@facadePattern');
Route::get('/proxy','HomeController@proxyPattern');


Route::get('/chain','HomeController@ChainOfResponsibilityPattern');
Route::get('/command','HomeController@commandPattern');
Route::get('/mediator','HomeController@mediatorPattern');
Route::get('/memento','HomeController@mementoPattern');
Route::get('/observer','HomeController@observerPattern');
Route::get('/state','HomeController@statePattern');
Route::get('/template','HomeController@templatePattern');
Route::get('/visitor','HomeController@visitorPattern');






