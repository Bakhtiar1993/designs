<?php

namespace App\Interfaces;

use App\Patterns\BehaviouralPatterns\Visitor\Group;
use App\Patterns\BehaviouralPatterns\Visitor\User;


/**
 * Note: the visitor must not choose itself which method to
 * invoke, it is the Visitee that make this decision
 */
interface RoleVisitorInterface
{
    public function visitUser(User $role);

    public function visitGroup(Group $role);
}