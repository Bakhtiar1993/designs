<?php

namespace App\Interfaces;


interface ComponentInterface
{
    public function operation();
    public function add(ComponentInterface $comOn);
    public function remove(ComponentInterface $comGone);
    public function getChild($someInt);
}