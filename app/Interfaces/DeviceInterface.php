<?php

namespace App\Interfaces;


interface DeviceInterface
{
    public function setSender(MessagingInterface $sender);
    public function send($body);
}