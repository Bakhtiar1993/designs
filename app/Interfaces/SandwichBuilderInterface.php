<?php

namespace App\Interfaces;


interface SandwichBuilderInterface
{
    public function setType();
    public function setNan();
    public function setMokhalafat();
    public function getResult();
}