<?php

namespace App\Interfaces;


interface ShapeInterface
{
    public function draw($height , $width);
}