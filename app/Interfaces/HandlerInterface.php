<?php

namespace App\Interfaces;


interface HandlerInterface
{
    public function setNext(HandlerInterface $handler);

    public function handle(string $request);
}