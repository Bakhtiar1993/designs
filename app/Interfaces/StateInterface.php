<?php

namespace App\Interfaces;


use App\Patterns\BehaviouralPatterns\State\OrderContext;

interface StateInterface
{
    public function proceedToNext(OrderContext $context);

    public function toString(): string;
}