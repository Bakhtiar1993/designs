<?php

namespace App\Interfaces;


interface MessagingInterface {

    public function send($body);
}