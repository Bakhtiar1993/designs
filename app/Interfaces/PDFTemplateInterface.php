<?php

namespace App\Interfaces;


interface PDFTemplateInterface
{
    public function renderTop();
    public function renderMiddle();
    public function renderBottom();
}