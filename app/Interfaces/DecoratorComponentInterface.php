<?php

namespace App\Interfaces;


interface DecoratorComponentInterface
{
    public function getInfo();
}