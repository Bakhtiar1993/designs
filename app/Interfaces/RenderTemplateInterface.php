<?php

namespace App\Interfaces;


interface RenderTemplateInterface
{
    public function renderHeader();
    public function renderBody();
    public function renderFooter();
}