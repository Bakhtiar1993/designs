<?php

namespace App\Interfaces;


interface FileInterface
{
    public function display();
}