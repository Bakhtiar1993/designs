<?php

namespace App\Interfaces;


interface ProductBuilderInterface
{
    public function getProperties();
}