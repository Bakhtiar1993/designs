<?php

namespace App\Interfaces;


interface RoleInterface
{
    public function accept(RoleVisitorInterface $visitor);

}