<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 06:24 PM
 */

namespace App\Patterns\BehaviouralPatterns\Mediator;


/**
 * Client is a client that makes requests and gets the response.
 */
class Client extends Colleague
{
    public function request()
    {
        $this->mediator->makeRequest();
    }

    public function output(string $content)
    {
        echo $content;
    }
}