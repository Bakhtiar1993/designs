<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 06:24 PM
 */

namespace App\Patterns\BehaviouralPatterns\Mediator;


class Database extends Colleague
{
    public function getData()
    {
        return 'World';
    }
}