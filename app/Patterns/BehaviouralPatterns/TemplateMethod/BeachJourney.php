<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/12/2019
 * Time: 10:49 AM
 */

namespace App\Patterns\BehaviouralPatterns\TemplateMethod;


class BeachJourney extends Journey
{
    protected function enjoyVacation(): string
    {
        return "Swimming and sun-bathing";
    }
}