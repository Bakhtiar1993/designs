<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 04:50 PM
 */

namespace App\Patterns\BehaviouralPatterns\ChainOfResponsibility;


use App\Interfaces\HandlerInterface;

abstract class AbstractHandler implements HandlerInterface
{

    private $nextHandler;

    public function setNext(HandlerInterface $handler)
    {
        $this->nextHandler = $handler;
        // Returning a handler from here will let us link handlers in a
        // convenient way like this:
        // $monkey->setNext($squirrel)->setNext($dog);
        return $handler;
    }

    public function handle(string $request)
    {
        if ($this->nextHandler) {
            return $this->nextHandler->handle($request);
        }

        return null;
    }
}