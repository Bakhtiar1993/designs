<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 04:50 PM
 */

namespace App\Patterns\BehaviouralPatterns\ChainOfResponsibility;


class DogHandler extends AbstractHandler
{
    public function handle(string $request)
    {
        if ($request === "MeatBall") {
            return "Dog: I'll eat the " . $request . ".\n";
        } else {
            return parent::handle($request);
        }
    }
}