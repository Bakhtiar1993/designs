<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/12/2019
 * Time: 11:08 AM
 */

namespace App\Patterns\BehaviouralPatterns\Visitor;


use App\Interfaces\RoleInterface;
use App\Interfaces\RoleVisitorInterface;

class User implements RoleInterface
{
    /**
     * @var string
     */
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function getName(): string
    {
        return sprintf('User %s', $this->name);
    }

    public function accept(RoleVisitorInterface $visitor)
    {
        $visitor->visitUser($this);
    }
}