<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/12/2019
 * Time: 09:45 AM
 */

namespace App\Patterns\BehaviouralPatterns\State;


use App\Interfaces\StateInterface;

class StateShipped implements StateInterface
{
    public function proceedToNext(OrderContext $context)
    {
        $context->setState(new StateDone());
    }

    public function toString(): string
    {
        return 'shipped';
    }
}