<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/12/2019
 * Time: 09:46 AM
 */

namespace App\Patterns\BehaviouralPatterns\State;


use App\Interfaces\StateInterface;

class StateDone implements StateInterface
{
    public function proceedToNext(OrderContext $context)
    {
        // there is nothing more to do
    }

    public function toString(): string
    {
        return 'done';
    }
}