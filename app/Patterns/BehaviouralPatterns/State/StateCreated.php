<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/12/2019
 * Time: 09:43 AM
 */

namespace App\Patterns\BehaviouralPatterns\State;


use App\Interfaces\StateInterface;

class StateCreated implements StateInterface
{
    public function proceedToNext(OrderContext $context)
    {
        $context->setState(new StateShipped());
    }

    public function toString(): string
    {
        return 'created';
    }
}