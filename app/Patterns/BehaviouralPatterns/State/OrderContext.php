<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/12/2019
 * Time: 09:42 AM
 */

namespace App\Patterns\BehaviouralPatterns\State;

use App\Interfaces\StateInterface;

class OrderContext
{
    /**
     * @var StateCreated
     */
    private $state;

    public static function create(): OrderContext
    {
        $order = new self();
        $order->state = new StateCreated();

        return $order;
    }

    public function setState(StateInterface $state)
    {
        $this->state = $state;
    }

    public function proceedToNext()
    {
        $this->state->proceedToNext($this);
    }

    public function toString()
    {
        return $this->state->toString();
    }
}