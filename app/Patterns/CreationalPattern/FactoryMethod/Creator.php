<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 07:07 PM
 */

namespace App\Patterns\CreationalPattern\FactoryMethod;


abstract class Creator
{
    protected abstract function factoryMethod();

    public function startFactory()
    {
        $mfg= $this->factoryMethod();
        return $mfg;
    }
}