<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 07:07 PM
 */

namespace App\Patterns\CreationalPattern\FactoryMethod;


use App\Interfaces\ProductBuilderInterface;

class GraphicProduct implements ProductBuilderInterface
{
    private $mfgProduct;
    public function getProperties()
    {
        $this->mfgProduct="This is a graphic.<3";
        return $this->mfgProduct;
    }
}