<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 07:07 PM
 */

namespace App\Patterns\CreationalPattern\FactoryMethod;


class TextFactory extends Creator
{
    protected function factoryMethod()
    {
        $product=new TextProduct();
        return($product->getProperties());
    }
}