<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 06:24 PM
 */

namespace App\Patterns\CreationalPattern\Singleton;


class MySingletonClass
{
    ///Condition 1 - Presence of a static member variable
    private static $_instance = null;

    ///Condition 2 - Locked down the constructor
    private function  __construct() { } //Prevent any oustide instantiation of this class

    ///Condition 3 - Prevent any object or instance of that class to be cloned
    private function  __clone() { } //Prevent any copy of this object

    ///Condition 4 - Have a single globally accessible static method
    public static function getInstance()
    {
        if( !is_object(self::$_instance) )  //or if( is_null(self::$_instance) ) or if( self::$_instance == null )
        {
            self::$_instance = new MySingletonClass();
            return self::$_instance;
        }
        return null;
    }


    ///Now we are all done, we can now proceed to have any other method logic we want

    //a simple method to echo something
    public function GreetMe()
    {
        echo 'Hello, this method is called by using a singleton object..';
    }
}

