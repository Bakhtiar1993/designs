<?php

namespace App\Patterns\CreationalPattern\FactoryPattern;


class ProductTable extends Product
{
    public  $width;

    public function setName($name)
    {
        $this->name=$name;
    }

    public function setWeight($weight)
    {
        $this->weight=$weight;
    }

    public function setWidth($width)
    {
        $this->width=$width;
    }

    public function getProduct()
    {
        return $this;
    }

}
