<?php

namespace App\Patterns\CreationalPattern\FactoryPattern;


class ProductBookcase extends Product
{
    public function __construct(){
        echo "ProductBookcase is class";
    }
}
