<?php

namespace App\Patterns\CreationalPattern\FactoryPattern;


class ProductChair extends Product
{
    public function __construct(){
        echo "ProductChair is class";
    }
}
