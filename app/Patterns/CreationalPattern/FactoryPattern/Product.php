<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 04:51 PM
 */

namespace App\Patterns\CreationalPattern\FactoryPattern;


abstract class Product
{
    public $weight;
    public  $name;

    public static function create($product_type)
    {

        switch($product_type)
        {
            case 'chair':

                return new ProductChair();

            case 'table':
                return new ProductTable();

            case 'bookcase':
                return new ProductBookcase();

        }

    }

}