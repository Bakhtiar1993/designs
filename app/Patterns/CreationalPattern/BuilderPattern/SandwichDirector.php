<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 04:21 PM
 */

namespace App\Patterns\CreationalPattern\BuilderPattern;

use App\Interfaces\SandwichBuilderInterface;

class SandwichDirector
{
    public function build(SandwichBuilderInterface $builder)
    {
        $builder->setType();
        $builder->setNan();
        $builder->setMokhalafat();

        return $builder->getResult();
    }
}