<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 04:21 PM
 */

namespace App\Patterns\CreationalPattern\BuilderPattern;


class Sandwich
{
    public $type;
    public $nan;
    public $mokhalafat;

    const SANDWICH_FELAFEL   = "Felafel";
    const SANDWICH_HAMBERGER   = "Hamberger";
    const SANDWICH_SOSIS= "Sosis";

    const SANDWICH_NAN_TOST= "Tost";
    const SANDWICH_NAN_GERD= "Gerd";
    const SANDWICH_NAN_DERAZ= "Deraz";

    const SANDWICH_MOKHALAFAT= "Mokhalafat";


}