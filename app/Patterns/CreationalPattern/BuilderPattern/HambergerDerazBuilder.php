<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 04:21 PM
 */

namespace App\Patterns\CreationalPattern\BuilderPattern;

use App\Interfaces\SandwichBuilderInterface;

class HambergerDerazBuilder implements SandwichBuilderInterface
{
    private $sandwich;

    public function __construct()
    {
        $this->sandwich = new Sandwich();
    }

    public function setType()
    {
        $this->sandwich->type = Sandwich::SANDWICH_HAMBERGER;
    }

    public function setNan()
    {
        $this->sandwich->nan = Sandwich::SANDWICH_NAN_DERAZ;
    }

    public function setMokhalafat()
    {
        $this->sandwich->mokhalafat = Sandwich::SANDWICH_MOKHALAFAT;
    }

    public function getResult()
    {
        return $this->sandwich;
    }
}