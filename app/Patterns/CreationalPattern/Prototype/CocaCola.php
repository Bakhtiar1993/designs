<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 05:59 PM
 */

namespace App\Patterns\CreationalPattern\Prototype;


class CocaCola {

    public $fizzy;
    public $healthy;
    public $tasty;

    /**
     * init a CocaCola drink
     */
    public function __construct() {
        $this->fizzy   = true;
        $this->healthy = false;
        $this->tasty   = true;
    }

    /**
     * This magic method is required, even if empty as part of the prototype pattern
     */
    public function __clone() {
    }

}