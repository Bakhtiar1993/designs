<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 11:01 AM
 */

namespace App\Patterns\StructuralPatterns\Decorator;

use App\Interfaces\DecoratorComponentInterface;

class DecoratorComponentUppercase implements  DecoratorComponentInterface
{
    public  $component;

    function __construct(DecoratorComponentInterface $component)
    {
        $this->component = $component;
    }

    public function getInfo()
    {
        echo strtoupper($this->component->getInfo());
    }
}