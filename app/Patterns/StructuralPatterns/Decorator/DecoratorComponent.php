<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 11:01 AM
 */

namespace App\Patterns\StructuralPatterns\Decorator;

use App\Interfaces\DecoratorComponentInterface;

class DecoratorComponent implements  DecoratorComponentInterface
{
    public function getInfo()
    {
        return 'ali HoSSein shAhabi' ;
    }
}