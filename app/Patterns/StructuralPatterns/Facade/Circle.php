<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 11:26 AM
 */

namespace App\Patterns\StructuralPatterns\Facade;


use App\Interfaces\ShapeInterface;

class Circle implements ShapeInterface {
    public function draw($height , $width){

        echo "Draw Circle ... $height * $width". "\n";
    }
}