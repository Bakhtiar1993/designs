<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 11:26 AM
 */

namespace App\Patterns\StructuralPatterns\Facade;

class FacadeClass {

    protected  static $height;
    protected  static $width;


    public static function drawShapes($height, $width)
    {
        self::$height=$height;
        self::$width=$width;

        self::drawRectangle(self::$height, self::$width);
        self::drawSquare(self::$height, self::$width);
        self::drawCircle(self::$height, self::$width);

    }

    private static function drawRectangle($height,$width)
    {
        $rectangle=new Rectangle();
        $rectangle->draw($height, $width);
    }
    private static function drawSquare($height,$width)
    {
        $rectangle=new Square();
        $rectangle->draw($height, $width);
    }
    private static function drawCircle($height,$width)
    {
        $rectangle=new Circle();
        $rectangle->draw($height, $width);
    }


}