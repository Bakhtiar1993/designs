<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 11:26 AM
 */

namespace App\Patterns\StructuralPatterns\Facade;


use App\Interfaces\ShapeInterface;

class Rectangle implements ShapeInterface {

    public function draw($height , $width){
//        Draw Rectangle
        echo "Draw Rectangle $height * $width  ...". "\n";
    }

}