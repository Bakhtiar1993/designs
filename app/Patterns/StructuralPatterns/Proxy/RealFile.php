<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 12:55 PM
 */

namespace App\Patterns\StructuralPatterns\Proxy;


use App\Interfaces\FileInterface;

class RealFile implements FileInterface
{
    private $fileName, $content;

    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
        $this->load($fileName);
    }

    public function load(string $fileName)
    {
        var_dump("real file");
        $this->content = $fileName." (file)";
    }

    public function display()
    {
        echo $this->content ."\n";
    }
}