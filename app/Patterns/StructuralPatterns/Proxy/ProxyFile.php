<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 12:55 PM
 */

namespace App\Patterns\StructuralPatterns\Proxy;


use App\Interfaces\FileInterface;

class ProxyFile  implements FileInterface
{
    private $realFile, $fileName;

    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    public function display()
    {
        if ($this->realFile == null){
            $this->realFile = new RealFile($this->fileName);
        }

        echo $this->realFile->display();
    }
}