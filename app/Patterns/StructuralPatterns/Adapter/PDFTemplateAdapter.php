<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 08:59 PM
 */

namespace App\Patterns\StructuralPatterns\Adapter;


use App\Interfaces\PDFTemplateInterface;
use App\Interfaces\RenderTemplateInterface;

class PDFTemplateAdapter implements RenderTemplateInterface
{
    private $pdfTemplate;

    public function __construct(PDFTemplateInterface $pdfTemplate)
    {
        $this->pdfTemplate = $pdfTemplate;
    }

    public function renderHeader()
    {
        return $this->pdfTemplate->renderTop();
    }

    public function renderBody()
    {
        return $this->pdfTemplate->renderMiddle();
    }

    public function renderFooter()
    {
        return $this->pdfTemplate->renderBottom();
    }
}