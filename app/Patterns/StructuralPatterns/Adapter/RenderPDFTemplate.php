<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 08:59 PM
 */

namespace App\Patterns\StructuralPatterns\Adapter;


use App\Interfaces\PDFTemplateInterface;

class RenderPDFTemplate implements PDFTemplateInterface
{

    public function renderTop()
    {
        return "This is the top of the PDF";
    }

    public function renderMiddle()
    {
        return "Hello World";
    }

    public function renderBottom()
    {
        return "This is the bottom of the PDF";
    }

}