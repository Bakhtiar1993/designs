<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 08:59 PM
 */

namespace App\Patterns\StructuralPatterns\Adapter;


use App\Interfaces\RenderTemplateInterface;

class RenderHTMLTemplate implements RenderTemplateInterface
{

    public function renderHeader()
    {
        return "";
    }

    public function renderBody()
    {
        return "Hello World";
    }

    public function renderFooter()
    {
        return "";
    }

}