<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 10:07 AM
 */

namespace App\Patterns\StructuralPatterns\Composite;


use App\Interfaces\ComponentInterface;

class Composite implements ComponentInterface
{
    private $sName;
    private $aChildren;

    public function __construct($sNodeName)
    {
        $this->sName=$sNodeName;
        $this->aChildren=[];
    }

    public function add(ComponentInterface $comOn)
    {
        array_push($this->aChildren,$comOn);
    }

    public function remove(ComponentInterface $comGone)
    {
        //Code to remove component
    }

    public function getChild($someInt)
    {
        //Code to get child by element value
    }

    //Note: The following method is recursive
    public function operation()
    {
        echo $this->sName . "<br />";
        foreach($this->aChildren as $elVal)
        {
            $elVal->operation();
        }
    }
}