<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/11/2019
 * Time: 10:07 AM
 */

namespace App\Patterns\StructuralPatterns\Composite;


use App\Interfaces\ComponentInterface;

class Leaf  implements ComponentInterface
{
    private $sName;

    public function __construct($sNodeName)
    {
        $this->sName=$sNodeName;
    }

    /* None of this batch of methods are used by Leaf */
    /* However in order to correctly implement the interface */
    /* you need some kind of implementation */

    public function add(ComponentInterface $comOn){}
    public function remove(ComponentInterface $comGone){}
    public function getChild($someInt){}

    /* Some useful content is required for the operation */

    public function operation()
    {
        echo $this->sName . "<br />";
    }
}