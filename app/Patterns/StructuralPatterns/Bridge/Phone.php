<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 09:22 PM
 */

namespace App\Patterns\StructuralPatterns\Bridge;



class Phone extends Device
{
    public function send($body)
    {
        $this->sender->send($body." send by phone");
    }
}