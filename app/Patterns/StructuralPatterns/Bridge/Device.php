<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 09:22 PM
 */

namespace App\Patterns\StructuralPatterns\Bridge;


use App\Interfaces\DeviceInterface;
use App\Interfaces\MessagingInterface;

abstract class Device implements DeviceInterface
{
    protected $sender;

    public function setSender(MessagingInterface $sender){
        $this->sender = $sender;
    }
}