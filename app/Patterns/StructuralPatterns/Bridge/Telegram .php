<?php
/**
 * Created by PhpStorm.
 * User: BR
 * Date: 05/10/2019
 * Time: 09:22 PM
 */

namespace App\Patterns\StructuralPatterns\Bridge;

use App\Interfaces\MessagingInterface;

class Telegram implements MessagingInterface
{
    public function send($body) {
        echo $body;
    }
}