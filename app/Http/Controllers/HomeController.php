<?php

namespace App\Http\Controllers;

use App\Interfaces\HandlerInterface;
use App\Patterns\BehaviouralPatterns\ChainOfResponsibility\DogHandler;
use App\Patterns\BehaviouralPatterns\ChainOfResponsibility\MonkeyHandler;
use App\Patterns\BehaviouralPatterns\ChainOfResponsibility\SquirrelHandler;
use App\Patterns\BehaviouralPatterns\Command\HelloCommand;
use App\Patterns\BehaviouralPatterns\Command\Invoker;
use App\Patterns\BehaviouralPatterns\Command\Receiver;
use App\Patterns\BehaviouralPatterns\Mediator\Client;
use App\Patterns\BehaviouralPatterns\Mediator\Database;
use App\Patterns\BehaviouralPatterns\Mediator\Mediator;
use App\Patterns\BehaviouralPatterns\Mediator\Server;
use App\Patterns\BehaviouralPatterns\Memento\State;
use App\Patterns\BehaviouralPatterns\Memento\Ticket;
use App\Patterns\BehaviouralPatterns\Observer\User;
use App\Patterns\BehaviouralPatterns\Observer\UserObserver;
use App\Patterns\BehaviouralPatterns\State\OrderContext;
use App\Patterns\BehaviouralPatterns\TemplateMethod\BeachJourney;
use App\Patterns\BehaviouralPatterns\TemplateMethod\CityJourney;
use App\Patterns\BehaviouralPatterns\Visitor\Group;
use App\Patterns\BehaviouralPatterns\Visitor\RoleVisitor;
use App\Patterns\CreationalPattern\BuilderPattern\FelafelDerazBuilder;
use App\Patterns\CreationalPattern\BuilderPattern\HambergerDerazBuilder;
use App\Patterns\CreationalPattern\BuilderPattern\SandwichDirector;
use App\Patterns\CreationalPattern\BuilderPattern\SosisDerazBuilder;
use App\Patterns\CreationalPattern\FactoryMethod\GraphicFactory;
use App\Patterns\CreationalPattern\FactoryMethod\TextFactory;
use App\Patterns\CreationalPattern\FactoryPattern\Product;
use App\Patterns\CreationalPattern\Prototype\CocaCola;
use App\Patterns\CreationalPattern\Singleton\MySingletonClass;
use App\Patterns\StructuralPatterns\Adapter\PDFTemplateAdapter;
use App\Patterns\StructuralPatterns\Adapter\RenderPDFTemplate;
use App\Patterns\StructuralPatterns\Bridge\Phone;
use App\Patterns\StructuralPatterns\Bridge\Telegram;
use App\Patterns\StructuralPatterns\Composite\Composite;
use App\Patterns\StructuralPatterns\Composite\Leaf;
use App\Patterns\StructuralPatterns\Decorator\DecoratorComponent;
use App\Patterns\StructuralPatterns\Decorator\DecoratorComponentUppercase;
use App\Patterns\StructuralPatterns\Facade\FacadeClass;
use App\Patterns\StructuralPatterns\Proxy\ProxyFile;

class HomeController extends Controller
{

    /* Creational Patterns */

    public function builderPattern()
    {
        $director                = new SandwichDirector();
        $sosisDerazBuilder     = new SosisDerazBuilder();
        $hambergerDerazBuilder     = new HambergerDerazBuilder();
        $felafelDerazBuilder     = new FelafelDerazBuilder();

        $sandwich= $director->build($hambergerDerazBuilder);
        return ['sandwich'=>$sandwich];
    }

    public function factoryPattern()
    {
        $table=Product::create('table');
        $table->setWeight(20);
        $table->setWidth(100);
        $table->setName('for ali asadi');

        return ['table'=>$table->getProduct()];
    }

    public function prototypePattern()
    {
        $cola = new CocaCola();
        $colaClone = clone $cola;

        return ['cola'=>$colaClone];
    }

    public function singletonPattern()
    {
        $obj1 = MySingletonClass::getInstance();
        $obj2 = MySingletonClass::getInstance();
        $obj3 = MySingletonClass::getInstance();

        $obj1->GreetMe();
        $obj2?$obj2->GreetMe():$this->echoMessage('Oops ,it is singleton ');
        $obj3?$obj3->GreetMe():$this->echoMessage('Oops ,it is singleton ');
    }

    public function factoryMethodPattern()
    {
        $someGraphicObject=new GraphicFactory();
        echo $someGraphicObject->startFactory() . "<br />";
        $someTextObject=new TextFactory();
        echo $someTextObject->startFactory() . "<br />";
    }

    /* Structural Patterns */

    public function adapterPattern()
    {
        $pdfTemplate = new RenderPDFTemplate();

        // $pdfTemplateAdapter will implement RenderTemplateInterface, just like RenderHTMLTemplate does
        $pdfTemplateAdapter = new PDFTemplateAdapter($pdfTemplate);

        // This is the top of the PDF
        echo $pdfTemplateAdapter->renderHeader();
    }

    public function bridgePattern()
    {
        $phone = new Phone();
        $phone->setSender(new Telegram);
        $phone->send("hello");
    }

    public function compositePattern()
    {
        $rootCompos = new Composite("Root");
        $n1=new Composite("-Composite 1");
        $n1->add(new Leaf("--C1:leaf 1"));
        $n1->add(new Leaf("--C1:leaf 2"));
        $rootCompos->add($n1);

        $n2=new Composite("-Composite 2");
        $n2->add(new Leaf("--C2:leaf 3"));
        $n2->add(new Leaf("--C2:leaf 4"));
        $n2->add(new Leaf("--C2:leaf 5"));
        $rootCompos->add($n2);

        $rootCompos->add(new Leaf("R:leaf 6"));

        //Create a node
        $rootCompos->operation();
    }

    public function decoratorPattern()
    {
        $component=new DecoratorComponent();
        echo $component->getInfo();

        echo '<br>';
        $decoratorComponent=new DecoratorComponentUppercase($component);
        $decoratorComponent->getInfo();
    }

    public function facadePattern()
    {
        FacadeClass::drawShapes('10','10');
        FacadeClass::drawShapes('20' ,'20');
    }

    public function proxyPattern()
    {
        $image = new ProxyFile('test.txt');
        $image->display();

        $image->display();
        $image->display();
        $image->display();
    }

    /* Behavioural Patterns */

    public function ChainOfResponsibilityPattern()
    {
        $monkey = new MonkeyHandler;
        $squirrel = new SquirrelHandler;
        $dog = new DogHandler;

        $monkey->setNext($squirrel)->setNext($dog);

        echo "Chain: Monkey > Squirrel > Dog\n\n";
        $this->clientCode($monkey);
        echo "\n";
    }

    public function commandPattern()
    {
        $invoker = new Invoker();
        $receiver = new Receiver();

        $invoker->setCommand(new HelloCommand($receiver));
        $invoker->run();
        echo $receiver->getOutput();
    }

    public function mediatorPattern()
    {
        $client = new Client();
        new Mediator(new Database(), $client, new Server());

         $client->request();
    }

    public function mementoPattern()
    {
        $ticket = new Ticket();

        // open the ticket
        $ticket->open();
        $openedState = $ticket->getState();
        echo State::STATE_OPENED ." :".(string) $ticket->getState()."\n";

        $memento = $ticket->saveToMemento();

        // assign the ticket
        $ticket->assign();
        echo State::STATE_ASSIGNED." :".(string) $ticket->getState()."\n";

        // now restore to the opened state, but verify that the state object has been cloned for the memento
        $ticket->restoreFromMemento($memento);
        echo State::STATE_OPENED." :".(string) $ticket->getState()."\n";
        echo $openedState." :".(string) $ticket->getState()."\n";
    }

    public function observerPattern()
    {
        $observer = new UserObserver();

        $user = new User();
        $user->attach($observer);

        $user->changeEmail('foo@bar.com');
        return   $observer->getChangedUsers();
    }

    public function statePattern()
    {
        $orderContext = OrderContext::create();

        echo "created :".$orderContext->toString()."\n";

        $orderContext->proceedToNext();
        echo "shipped :".$orderContext->toString()."\n";

        $orderContext->proceedToNext();
        echo "done :".$orderContext->toString()."\n";

        $orderContext->proceedToNext();
        echo "what? ".$orderContext->toString()."\n";
    }

    public function templatePattern()
    {
        $beachJourney = new BeachJourney();
        $beachJourney->takeATrip();

        echo 'Buy a flight ticket ,Taking the plane ,Swimming and sun-bathing , Taking the plane'."\n";
        var_dump($beachJourney->getThingsToDo());

        echo "\n";
        $cityJourney = new CityJourney();
        $cityJourney->takeATrip();

        echo 'Buy a flight ticket ,Taking the plane ,Eat, drink, take photos and sleep ,Buy a gift , Taking the plane'."\n";
        var_dump($cityJourney->getThingsToDo());
    }

    public function visitorPattern()
    {
        $visitor = new RoleVisitor();

        var_dump( [
            [new App\Patterns\BehaviouralPatterns\Visitor\User('Dominik')],
            [new Group('Administrators')],
        ]);
//        $role=new Role();
//        $role->accept($this->visitor);
    }

    private function clientCode(HandlerInterface $handler)
    {
        foreach (["Nut", "Banana", "Cup of coffee"] as $food) {
            echo "Client: Who wants a " . $food . "?\n";
            $result = $handler->handle($food);
            if ($result) {
                echo "  " . $result;
            } else {
                echo "  " . $food . " was left untouched.\n";
            }
        }
    }

    private function echoMessage($msg='Oops')
    {
        echo $msg;
    }

}
